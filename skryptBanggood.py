# version 2.0

from bs4 import BeautifulSoup
import urllib2

# categories links, feel free to add one or more
electronic = 'http://www.banggood.com/Wholesale-Electronics-c-1091-0-1-1-45-0_page'
phones = 'http://www.banggood.com/Wholesale-Cell-Phones-and-Accessories--c-140-0-1-1-45-0_page'
lights = 'http://www.banggood.com/Wholesale-Lights-and-Lighting-c-1697-0-1-1-45-0_page'
computers = 'http://www.banggood.com/Wholesale-Computer-and-Networking-c-155-0-1-1-45-0_page'

# general settings, do not touch this!
blue = 'http://img.banggood.com/images/activity/logo/2016/0303/201308.png'
yellow = 'http://img.banggood.com/images/activity/logo/2016/0303/041243.png'
turquoise = 'http://img.banggood.com/images/activity/logo/2016/0303/201238.png'
lightBlue = 'http://img.banggood.com/images/activity/logo/2016/0303/201211.png'
red = 'http://img.banggood.com/images/activity/logo/2016/0303/192028.png'

endOfLink = '.html'

bluePack = []
yellowPack = []
turquoisePack = []
lightBluePack = []
redPack = []

# settings that you must customize
maxPages = 4
chosenCategory = electronic

def packToColor(pack):
    if(pack == bluePack):
        return "Blue:"
    if(pack == yellowPack):
        return "Yellow:"
    if(pack == turquoisePack):
        return "Turquoise:"
    if(pack == lightBluePack):
        return "Blue light:"
    if(pack == redPack):
        return "Red:"
        
def printer(pack):
    print '\n' 
    print packToColor(pack)
    print '\n'
    for element in pack:
        print element
    return 

def main():
    # finding script
    currentPage = 0

    for page in range(0,maxPages):
        print '\n'    
        print 'Processing page ' + str(currentPage) + '...'
        currentLink = chosenCategory + str(currentPage) + endOfLink
        currentPage = currentPage + 1
        response = urllib2.urlopen(currentLink)
        soup = BeautifulSoup(response, "html.parser")
        images = soup.findAll('span', {'class' : 'img'})
        for element in images:    
            uElement = element.findAll('u',{'class' : 'easter'})
            if(len(uElement) != 0):
                links = element.findAll('a', href=True)
                for link in links:
                    imgElement = element.findAll('img')
                    for i in imgElement:
                        if(i['src'] == blue):
                            bluePack.append(link['href'])    
                        if(i['src'] == yellow):
                            yellowPack.append(link['href'])
                        if(i['src'] == turquoise):
                            turquoisePack.append(link['href'])
                        if(i['src'] == lightBlue):
                            lightBluePack.append(link['href'])
                        if(i['src'] == red):
                            redPack.append(link['href'])
    printer(bluePack)
    printer(yellowPack)
    printer(turquoisePack)
    printer(lightBluePack)
    printer(redPack)
        
        
if __name__ == "__main__":
    main()        